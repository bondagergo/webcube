import * as mongodb from "mongodb"

const connectionString = 'mongodb+srv://Omnius:ellipsis1989@mongobg.ajkbv.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

const mongoCLI = mongodb.MongoClient;


async function Connect (): Promise<mongodb.MongoClient | undefined> { //

    try {
        
         const client = await mongoCLI.connect(connectionString) //,{useNewUrlParser: true}
         console.log("Connected to MongoDB")
         return client
        
    } catch (error) {
        console.error(error)

        return undefined
        
    }
    
}

export default Connect;