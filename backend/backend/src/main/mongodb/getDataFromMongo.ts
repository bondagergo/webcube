//import { FindCursor, WithId, Document } from "mongodb";
import { Db } from "mongodb";
import Connect from "./connector";



export async function simplequery(query:string): Promise<any> {  //
    const mongoCli = await Connect()
    const queryObject = {}
    //const regexIsNumber = '/[^.0-9]+/g'
    if(query.includes("=")){

        const key = query.split("=")[0].trim()
        const value = query.split("=")[1]
        const numberNames = ["size","price","numberOfRooms"]
        if(numberNames.some(v => key.includes(v))){
            queryObject[key] = +value
        }
        else{
            queryObject[key] = value.trim()
        }
    }
    if(query.includes("<")){
        const key = query.split("<")[0].trim()
        const value = query.split("<")[1]
        queryObject[key] = {$lt:  +value}
    }
    if(query.includes(">")){
        const key = query.split(">")[0].trim()
        const value = query.split(">")[1]
        queryObject[key] = {$gt: +value}
    }
    
    const ingatlanCom = mongoCli?.db("ingatlanok")
    console.log(queryObject)
    const results = await getResults(ingatlanCom,queryObject);
    mongoCli?.close()


    return results
}

async function getResults(db: Db | undefined,query: Object) {
    return db?.collection('ingatlancom').find(query).toArray();
}

//simplequery({numberOfRooms:"4"})





