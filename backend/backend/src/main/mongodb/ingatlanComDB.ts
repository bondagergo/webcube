import Connect from "./connector"


export async function SaveToMongo(saveData) {
    const mongoCli = await Connect()
    const ingatlanok = mongoCli?.db("ingatlanok")
    const ingatlancom = ingatlanok?.collection("ingatlancom")
    await ingatlancom?.insertMany(saveData)
    console.log("Insert Finished")
}
