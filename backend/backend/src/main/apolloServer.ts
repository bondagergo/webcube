import { ApolloServer } from 'apollo-server-express'
import { schema } from "./graphql/gqFeedFunFood"
import * as express from "express"

export class ApolloServerStart {
    server: ApolloServer

    constructor(){
        this.server = new ApolloServer({
            schema,
          })
          
    }
     async serverStart(app: express.Application, path: string) {
        await this.server.start();
        return this.server.applyMiddleware({ app, path: path })
    }

}