import Connect from "./connector";
import * as mongoose from 'mongoose';

export async function simplequery(query:Object, db:string,collection: string, dataSchema: mongoose.Schema<any>): Promise<any> { 
    await Connect(db)
    console.log('DB')
    const resultArray: Array<any> = []
    const modelToGet =  mongoose.model(collection,dataSchema);
    console.log('Model Created')
    const result = await modelToGet.find(query).exec()
    console.log(result.length)
    //const returnResult = result.map(r =>{r.toObject()})
    result.forEach(r => {resultArray.push(r.toObject())})
    console.log('Here')
    console.log(resultArray)
    return resultArray
}