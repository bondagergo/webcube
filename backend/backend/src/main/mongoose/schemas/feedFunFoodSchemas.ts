import * as mongoose from 'mongoose';

const foodSchema = new mongoose.Schema(
{
    id: {type: Number, required: false },
    name: {type: String, required: false},
    pictures: {type: [String], required:false},
    ingredientIds: {type: [Number], required: false},
    meal: {type: String, required: false},
    owner: {type: String, required: false}
},
    {collection:'foods'}
)

const daySchema = new mongoose.Schema(
    {
        id: {type: Number, required: false },
        date: {type: String, required: false},
        foodid:{type: [Number], required: false}
    },
        {collection:'days'}
    )

const ingredientsSchema = new mongoose.Schema(
    {
        id: {type: Number, required: false },
        name: {type: String, required: false},
        season:{type: String, required: false},
        foodid: {type: [Number], required: false}
    },
    {collection: 'ingredients'}
)

export  {foodSchema, daySchema, ingredientsSchema};