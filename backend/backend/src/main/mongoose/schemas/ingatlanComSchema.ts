import * as mongoose from 'mongoose';
import {ingatlancomPage} from '../../services/ingatlanComParserNew'


const ingatlanComSchema = new mongoose.Schema<ingatlancomPage>({
    link: {type: String, required: false },
    name: {type: String, required: false },
    type: {type: String, required: false },
    subtype: {type: String, required: false },
    price: {type: Number, required: false },
    size: {type: Number, required: false },
    lotSize: {type: String, required: false },
    gardenSize: {type: String, required: false },
    numberOfRooms: {type: Number, required: false },
    propertyStatus: {type: String, required: false },
    buildYear: {type: String, required: false },
    comfort: {type: String, required: false },
    energyLevel: {type: String, required: false },
    level: {type: String, required: false },
    hasBasement: {type: Boolean, required: false },
    maxLevel: {type: String, required: false },
    elevator: {type: Boolean, required: false },
    innerHeight: {type: String, required: false },
    heatingType: {type: String, required: false },
    airCondition: {type: Boolean, required: false },
    overhead: {type: String, required: false },
    accessible: {type: Boolean, required: false },
    bathroomToilet: {type: String, required: false },
    orientation: {type: String, required: false },
    view: {type: String, required: false },
    balconySize: {type: String, required: false },
    gardenConnection: {type: String, required: false },
    attic: {type: String, required: false },
    parking: {type: String, required: false },
    isGreenHome: {type: Boolean, required: false },
    longDescription:{type: String, required: false },
    photos:{type: Object, required: false },
    sellerName: {type: String, required: false },
    sellerPhone: {type: String, required: false } },
    { collection : 'ingatlancom' });

    export default ingatlanComSchema;


