import console = require("console")
import { SaveToMongo } from "../saveToMongo"
import { daySchema, foodSchema, ingredientsSchema } from "../schemas/feedFunFoodSchemas"

class saveFeedFunFood {
    constructor() {
        console.log('Valami?')
        this.saveDays();
        this.saveFoods();
        this.saveIngredients();
    }

    async  saveFoods() {
        const parsedArray: Array<Object> = [
            {
                id: 1,
                name: 'Bableves',
                pictures: ['https://kep.cdn.indexvas.hu/1/0/1941/19413/194139/19413903_0ee28a7651fcbc879210d939d2effd49_wm.jpg'],
                ingredientIds: [1,2,3,4],
                meal: 'ebéd'
            },
            {
                id: 2,
                name: 'Túrógombóc',
                pictures: ['https://image-api.nosalty.hu/nosalty/images/recipes/lM/ba/turogomboc-3.jpeg?w=640&h=640&fit=crop&s=1819b48bf535952e35772022e28eba97'],
                ingredientIds: [4,5,6,7],
                meal: 'ebéd'
            },

        ]
        console.log('Started')
        await SaveToMongo(parsedArray,'feedfunfood','foods',foodSchema)
    }

    async  saveDays() {
        const parsedArray: Array<Object> = [
            {
                id: 1,
                date: '2022.04.20',
                foodid: [1]
            },
            {
                id: 2,
                date: '2022.04.20',
                foodid: [2]
            },
            {
                id: 3,
                date: '2022.04.21',
                foodid: [1]
            },
            {
                id: 4,
                date: '2022.04.22',
                foodid: [1,2]
            },

        ]

        await SaveToMongo(parsedArray,'feedfunfood','days',daySchema)
    }

    async  saveIngredients() {
        const parsedArray: Array<Object> = [
            {
                id: 1,
                name: 'bab',
                season: 'all',
                foodid: [1]
            },
            {
                id: 2,
                name: 'hagyma',
                season: 'all',
                foodid: [1]
            },
            {
                id: 3,
                name: 'kolbász',
                season: 'all',
                foodid: [1]
            },
            {
                id: 4,
                name: 'tejföl',
                season: 'all',
                foodid: [1,2]
            },
            {
                id: 5,
                name: 'túró',
                season: 'all',
                foodid: [2]
            },
            {
                id: 6,
                name: 'prézli',
                season: 'all',
                foodid: [2]
            },
            {
                id: 7,
                name: 'cukor',
                season: 'all',
                foodid: [2]
            },

        ]

        await SaveToMongo(parsedArray,'feedfunfood','ingredients',ingredientsSchema)
    }

}
const x = new saveFeedFunFood();
x.saveFoods().then()
