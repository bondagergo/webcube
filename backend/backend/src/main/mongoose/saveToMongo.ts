import * as mongoose from "mongoose"
import Connect from "./connector"

export async function SaveToMongo(saveData: Array<Object>,dbName: string,collectionName: string, dataSchema: mongoose.Schema<any>) {
    /*const mongooseCli =*/ await Connect(dbName)

    const ModelToSave = mongoose.model(collectionName, dataSchema);
    await ModelToSave.insertMany(saveData)
    console.log("Insert Finished")
}