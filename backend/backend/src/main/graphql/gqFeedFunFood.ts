import {GraphQLSchema,GraphQLObjectType,GraphQLString,GraphQLList,GraphQLInt,GraphQLNonNull} from 'graphql'
import { simplequery } from '../mongoose/getDataFromMongo'
import {SaveToMongo} from '../mongoose/saveToMongo'
import { daySchema, foodSchema, ingredientsSchema } from '../mongoose/schemas/feedFunFoodSchemas'



const FoodType = new GraphQLObjectType({
  name: 'Food',
  description: 'This represents a food we eat',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLInt) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    pictures: { type: new GraphQLList(GraphQLString) },
    ingredientIds: {type: new GraphQLList(GraphQLInt)},
    ingredients: {type: new GraphQLList(IngredientType),
                  resolve: async (food) => {
                    const queryObject = {id: {$in:
                      food.ingredientIds
                    }}
  
                     return await simplequery(queryObject,'feedfunfood','ingredients',ingredientsSchema)
                  }},
    meal: {type: new GraphQLNonNull(GraphQLString)},
    owner: {type: new GraphQLNonNull(GraphQLString)},
    date: {
      type: new GraphQLList(DayType) ,
      resolve: async (food) => {
        const queryObject = {foodid: {$in:[
          food.ingredientIds
        ]}}
        return await simplequery(queryObject,'feedfunfood','days',daySchema)
      }
    }
  })
})

const DayType = new GraphQLObjectType({
  name: 'Day',
  description: 'This represents a day of feeding',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLInt) },
    date: { type: new GraphQLNonNull(GraphQLString) },
    foodid: {type: new GraphQLNonNull(GraphQLInt)},
    foodname: {
      type: new GraphQLList(FoodType),
      resolve: async (day) => {
        //return books.filter(book => book.authorId === author.id)
        const queryObject = {id: day.foodid}// Itt egy listás keresés kell 
        return await simplequery(queryObject,'feedfunfood','food',foodSchema)
      }
    }
  })
})

const IngredientType = new GraphQLObjectType({
  name: 'Ingredient',
  description: 'This represents an ingredient',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLInt) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    season: {type: new GraphQLNonNull(GraphQLString)},
    foodids: {type: new GraphQLList(GraphQLInt)},
    foodnames: {
      type: new GraphQLList(FoodType),
      resolve: async (ingredient) => {
        //return books.filter(book => book.authorId === author.id)
        const queryObject = {foodid: {$in:[
          ingredient.ingredientIds
        ]}}
        return await simplequery(queryObject,'feedfunfood','food',foodSchema)
      }
    }
  })
})

const RootQueryType = new GraphQLObjectType({
  name: 'Query',
  description: 'Root Query',
  fields: () => ({
    food: {
      type: FoodType,
      description: 'A Single Food',
      args: {
        id: { type: GraphQLInt }
      },
      resolve: async (parent, args) => {const x = await simplequery({id: args.id},'feedfunfood','foods',foodSchema)
    return x[0]}
    },
    foods: {
      type: new GraphQLList(FoodType),
      description: 'List of All Foods',
      resolve: async () => {return await simplequery({},'feedfunfood','foods',foodSchema)}
    },
    days: {
      type: new GraphQLList(DayType),
      description: 'List of All Days',
      resolve: async () => {return await simplequery({},'feedfunfood','days',daySchema)}
    },
    day: {
      type: DayType,
      description: 'A Single Day',
      args: {
        id: { type: GraphQLInt }
      },
      resolve: async (parent, args) => {const x = await simplequery({id: args.id},'feedfunfood','days',daySchema)
      return x[0]}
    },
    ingredients: {
      type: new GraphQLList(IngredientType),
      description: 'List of All Ingredients',
      resolve: async () => {return await simplequery({},'feedfunfood','ingredients',ingredientsSchema)}
    },
    ingredient: {
      type: IngredientType,
      description: 'A Single Ingredient',
      args: {
        id: { type: GraphQLInt }
      },
      resolve: async (parent, args) => {const x = await simplequery({id: args.id},'feedfunfood','ingredients',ingredientsSchema)
      return x[0]}
    }
  })
})

const RootMutationType = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Root Mutation',
  fields: () => ({
    addFood: {
      type: FoodType,
      description: 'Add a food',
      args: {
        id: {type: new GraphQLNonNull(GraphQLInt)},
        name: { type: new GraphQLNonNull(GraphQLString) },
        pictures: { type: new GraphQLList(GraphQLString) },
        ingredientIds: {type: new GraphQLList(GraphQLInt)},
        ingredientNames: {type: new GraphQLList(GraphQLString)},
        meal: {type: new GraphQLNonNull(GraphQLString)},
        owner: {type: new GraphQLNonNull(GraphQLString)}
      },
      resolve: async (parent, args) => {
        //model.find({ ... }).sort({ field : criteria}).exec(function(err, model){ ... });
        const queryObject = {name: {$in:args.ingredientNames}}
        
        const ingIds =  await simplequery(queryObject,'feedfunfood','ingredients',ingredientsSchema)

        const IdArray: number[] = []
        ingIds.forEach(element => {IdArray.push(element.id)
          
        });
        console.log(IdArray)
        const objectToAdd = [{id:args.id,name:args.name,pictures:args.pictures,
                            ingredientIds:IdArray,meal:args.meal,owner:args.owner}]
        await SaveToMongo(objectToAdd,'feedfunfood','foods',foodSchema)
      }
    },
    addDay: {
      type: DayType,
      description: 'Add a day',
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) }
      },
      resolve: (parent, args) => {
        //const author = { id: authors.length + 1, name: args.name }
        //authors.push(author)
        //return author
      }
    },
    addIngredient: {
      type: IngredientType,
      description: 'Add an ingredient',
      args: {
        id: {type: new GraphQLNonNull(GraphQLInt)},
        name: { type: new GraphQLNonNull(GraphQLString) },
        season: {type: new GraphQLNonNull(GraphQLString)},
        foodid: {type: new GraphQLList(GraphQLInt)},
        foodnames: {type: new GraphQLList(GraphQLString)}
      },
      resolve: async (parent, args) => {
        const queryObject = {name: {$in:args.foodnames}}
        const foodIds: any[] =  await simplequery(queryObject,'feedfunfood','foods',foodSchema)
        const IdArray: number[] = []
        foodIds.forEach(element => {IdArray.push(element.id)
          
        });
        console.log(IdArray)
        const objectToAdd = [{id:args.id,name:args.name,season:args.season,
                            foodid:IdArray}]
        await SaveToMongo(objectToAdd,'feedfunfood','ingredients',ingredientsSchema)
      }
    }
  })
})



export const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType
})