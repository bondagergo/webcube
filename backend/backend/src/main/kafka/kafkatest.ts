import { Kafka } from 'kafkajs';


const kafkaBroker: string =  process.env.KAFKA_ZOOKEEPER_CONNECT || '127.0.0.1:9092'

const kafka = new Kafka({
  clientId: 'my-app',
  brokers: [kafkaBroker],
})


export async function kafkaProduce() {
    console.log(kafkaBroker)
    const producer = kafka.producer()

    await producer.connect()
    await producer.send({
    topic: 'test-topic',
    messages: [
        { value: 'Hello KafkaJS user!' },
    ],
    })

    await producer.disconnect()
}

export async function kafkaConsume() {
    const consumer = kafka.consumer({ groupId: 'test-group' })

    await consumer.connect()
    await consumer.subscribe({ topic: 'test-topic', fromBeginning: true })

    await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {

        if (message.value != null) {
            console.log({
                value: message.value.toString(),
                })
        }
        
    },
    })
}