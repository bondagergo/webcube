//src/index.ts
import { Kafka, KafkaConfig } from 'kafkajs';

export async function sendChatMessage(sender:string,topic:string,messageContent: string, newTopic: boolean) {
    // newtopic boolean param bevezetése ami szerint a clientId vagy töltött vagy nem
    // Az egész new Kafka param objectet szétválasztani eszerint
    const kafkaBroker: string =  process.env.KAFKABROKER || '127.0.0.1:9092'
    var kafkaObject: KafkaConfig = {brokers:[]}
    if (newTopic){
         kafkaObject = {
            clientId: topic,
            brokers: [kafkaBroker]
       }
    }
    else{
        kafkaObject = {
            brokers: [kafkaBroker]
       }

    }
    
    
    const kafka = new Kafka(kafkaObject);

    const producer = kafka.producer();

    producer.connect().then(() => {
    return producer.send({
        topic: topic,
        messages: [
        { value: JSON.stringify({ message:messageContent, sender: sender }) },
        ],
    }).then(() => {
        console.log('Message sent');
        return producer.disconnect();
    });
})


}

