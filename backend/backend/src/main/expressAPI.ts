import * as express from "express"
//import * as expressGraphQL from "express-graphql"
import { Application } from "express"
import { GoogleMapRoutesController } from "./controller/GoogleMapRoutesController"
import { HanoiStepsController } from "./controller/HanoiStepsController"
import { KafkaSendController } from "./controller/KafkaSendController"
import { RealEstateController } from "./controller/RealEstateController"
import { WebScraperController } from "./controller/WebScraperController"
import { WebScraperSingleaController } from "./controller/WebScraperSingleController"
//import { ApolloServer } from 'apollo-server-express'
//import { schema } from "./graphql/gqFeedFunFood"
import { ApolloServerStart } from "./apolloServer"

//const FRONTEND_PATH = path.join(__dirname, "../../frontend/build")
const expressAPI =  (): Application =>
{

  
    const app = express()
    //const expressGraphQL = require('express-graphql')



    const hanoiStepsController = new HanoiStepsController()
    const webscraperController = new WebScraperController()
    const webScraperSingleaController = new WebScraperSingleaController()
    const realEstateController = new RealEstateController()
    const kafkaSendController = new KafkaSendController()
    const googleMapRoutesController = new GoogleMapRoutesController()
    const apolloServer = new ApolloServerStart()

    

    app.use("/hanoisteps",hanoiStepsController.router)
    app.use("/webscraper",webscraperController.router)
    app.use("/realestates",realEstateController.router)
    app.use("/kafkachat",kafkaSendController.router)
    app.use("/googlemaproutes",googleMapRoutesController.router)
    app.use("/webscrapersingle",webScraperSingleaController.router)
    apolloServer.serverStart(app, '/graphql')
    //server.applyMiddleware({ app, path: '/graphql' })
    //app.use("/feedfunfood", expressGraphQL({schema: schema,graphiql: true}))

    

    /* static assets and homepage */
    //app.use(express.static(FRONTEND_PATH))
    //app.get("*", (req: Request, res: Response) => res.sendFile(FRONTEND_PATH + "/index.html"))


    return app
}

export {expressAPI}