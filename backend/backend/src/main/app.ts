//import * as config from "../config/default.js"
import {expressAPI} from "./expressAPI"
import {expressBase} from "./expressBase"


//const port = config.get('port') as number
//const host = config.get('host') as string
const port = 1337
const host = 'localhost'
//const expressAPI = require('./expressAPI')
//const expressBase = require('./expressBase')
export const apiApp =  expressAPI()
export const app =   expressBase(apiApp)

//await new Promise(resolve => 
//    app.listen(port, () => {
//        console.log(`Server listing at http://${host}:${port}`)
//    })
//    );


app.listen(port, () => {
    console.log(`Server listing at http://${host}:${port}`)
})