import {getRequest} from "../services/httpRequest.js"
import {  parseSearchResult} from "./ingatlancComParser.js"
import {ingatlancomPage, parseRealEstatePage} from "./ingatlanComParserNew.js"

export async function linkCollector(urlToSave:string) {
        
        
        const pageHtml = getRequest(urlToSave)
        const {returnedLinks, maxnumber} = parseSearchResult(await pageHtml) //maxNumber
        const linksToVisit = returnedLinks
        return {linksToVisit, maxnumber }

}

export async function parsePages(linksToVisit:Array<string | undefined>): Promise<ingatlancomPage[]> {
    const parsedPage: Array<ingatlancomPage> = []




   // await Promise.all(linksToVisit.map(async (element,index) =>{
        
        
        
        
   // }))

    for (const element of linksToVisit)
    {
        const pageHtml = await getRequest("https://ingatlan.com" + element)
        //await sleep(2000)
        const parsedResult = await parseRealEstatePage("https://ingatlan.com" + element, pageHtml)
        console.log('Done')
        //console.log(parsedResult)
        parsedPage.push(parsedResult)
    }
    

return parsedPage
}

//async function sleep(miliseconds:number) {

//    return new Promise(resolve => setTimeout(resolve,miliseconds))
    
//}