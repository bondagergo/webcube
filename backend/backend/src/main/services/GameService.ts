export function SolveSteps (steps: Array<string>,towerlvl: number,startTower: string,endTower: string, helpTower: string) :Array<string>
{   //var steps: Array<string> = []
    var endString: string = ""
    if(towerlvl === 1)
        {
            endString = `Move disc ${towerlvl} to ${endTower} from ${startTower}`
            steps.push(endString)
        }
    else
    {
        SolveSteps(steps,towerlvl -1, startTower,helpTower,endTower)
        endString = `Move disc ${towerlvl} to ${endTower} from ${startTower}`
        steps.push(endString)
        SolveSteps(steps,towerlvl -1, helpTower,endTower,startTower)
    }
    return steps
}


