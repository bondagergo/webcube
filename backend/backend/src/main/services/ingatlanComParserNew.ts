import * as cheerio from "cheerio"
import { getRequest } from "./httpRequest.js"

export interface ingatlancomPage {
    link: string,
    name: string,
    type: string,
    subtype: string,
    price: number,
    size: number,
    lotSize: string,
    gardenSize: string,
    numberOfRooms: number,
    propertyStatus: string,
    buildYear: string,
    comfort: string,
    energyLevel: string,
    level: string,
    hasBasement: Boolean,
    maxLevel: string,
    elevator: Boolean,
    innerHeight: string,
    heatingType: string,
    airCondition: Boolean,
    overhead: string,
    accessible: Boolean,
    bathroomToilet: string,
    orientation: string,
    view: string,
    balconySize: string,
    gardenConnection: string,
    attic: string,
    parking: string,
    isGreenHome: Boolean,
    longDescription:string,
    photos:Object,
    sellerName: string,
    sellerPhone: string

}

export async  function parseRealEstatePage(url: string, html: string): Promise<ingatlancomPage> { 
    //let pageHtml = await getRequest(url)
    const $ = cheerio.load(html)

    const listingData = $("#listing").prop("data-listing")
    const locationHierarchy = $("#listing").prop("data-location-hierarchy")
    const sellerData = $("#listing").prop("data-seller")

    const parsedListingData = JSON.parse(listingData)
    const parsedLocationHierarchy = JSON.parse(locationHierarchy)
    const parsedSellerData = JSON.parse(sellerData)

    /* Phone Number */
    const sellerId = parsedSellerData.id
    const sellerTelInfo: any = await getRequest("https://partner.ingatlan.com/telefonszam/" + sellerId) 
    const sellerPage = await getRequest(parsedSellerData.websiteUrl)
    const $$ = cheerio.load(sellerPage)
    const firstNumberPart = $$("div.phone-contact__main-number").prop("innerHTML")?.split('\n')[1].trim()
    const secondNumberPart = sellerTelInfo.phoneNumbers[Object.keys(sellerTelInfo.phoneNumbers)[0]]
    /*---------------------------*/
    console.log(parsedListingData.property)
    console.log(typeof parsedListingData.property)
    const parsedProperty: ingatlancomPage = parsedListingData.property
    const parsedPropertyParking = parsedProperty.hasOwnProperty("parking") && parsedListingData.parking != null ?
                                      parsedListingData.property.parking : ""

     const parsedPage: ingatlancomPage = {
        link: url.trim(),
        name: parsedLocationHierarchy.locations[1].name + "," + parsedLocationHierarchy.locations[0].name,
        type: parsedListingData.property.type,
        subtype: parsedListingData.property.subtype,
        price: +parsedListingData.prices[0].amount || 0,
        size: +parsedListingData.property.areaSize || 0,
        lotSize: parsedListingData.property.lotSize,
        gardenSize:parsedListingData.property.gardenSize,
        numberOfRooms: +parsedListingData.property.roomCount || 0,
        propertyStatus: parsedListingData.property.condition || "",
        buildYear: parsedListingData.property.yearOfConstruction || "",
        comfort: parsedListingData.property.comfortLevel || "",
        energyLevel: parsedListingData.property.energyEfficiencyRating || "",
        level: parsedListingData.property.floor || "",
        hasBasement: parsedListingData.property.hasBasement, //bool
        maxLevel: parsedListingData.property.buildingFloorCount || "",
        elevator: parsedListingData.property.hasElevator, //bool
        innerHeight: parsedListingData.property.innerHeight || "",
        heatingType: parsedProperty.heatingType != null && parsedListingData.property.heatingTypes.length > 0 ? parsedListingData.property.heatingTypes[0] : "", //array
        airCondition: parsedListingData.property.hasAirConditioner || false, //bool
        overhead: "",
        accessible: parsedListingData.property.hasBarrierFreeAccess, //bool
        bathroomToilet: parsedListingData.property.bathroomToiletSeparation || "",
        orientation: parsedListingData.property.orientation || "",
        view: parsedListingData.property.view,
        balconySize: parsedListingData.property.balconySize,
        gardenConnection: parsedListingData.property.hasGardenAccess,
        attic: parsedListingData.property.atticType,
        parking: parsedProperty.hasOwnProperty("parking") && parsedListingData.parking != null ? (parsedPropertyParking.hasOwnProperty("type") && parsedListingData.property.parking.type != null ? parsedListingData.property.parking.type : "") : "" ,
        isGreenHome:parsedListingData.property.isGreenHome, //bool
        longDescription: parsedListingData.property.description,
        photos: parsedListingData.photos || [],
        sellerName: parsedSellerData.name,
        sellerPhone: (firstNumberPart + " " + secondNumberPart) || ""

    }
return parsedPage
//console.log(parsedListingData.photos[0].urls)
//console.log(parsedLocationHierarchy)
}