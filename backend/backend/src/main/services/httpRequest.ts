import * as needle from "needle"
import * as fs from "fs"


export async function getRequest(url:string): Promise<string> {

    try {
        


        const result = await needle('get', url,
        {headers:{"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
                  "Accept":"*/*",
                  "Accept-Encoding":"gzip, deflate, br"},
        open_timeout: 0})
        return result.body
    } catch (error) {
        console.log("getRequest: " + url)
        console.log(error)
        return ""
    }
    
    
}

export function saveHtml(html:string) {
    fs.writeFileSync('./test.html',html)
    
}

