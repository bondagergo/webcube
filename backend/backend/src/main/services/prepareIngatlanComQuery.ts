export function prepareIngatlanComQuery(query:string): Object
{

    const queryObject = {}
    //const regexIsNumber = '/[^.0-9]+/g'
    if(query.includes("=")){

        const key = query.split("=")[0].trim()
        const value = query.split("=")[1]
        const numberNames = ["size","price","numberOfRooms"]
        if(numberNames.some(v => key.includes(v))){
            queryObject[key] = +value
        }
        else{
            queryObject[key] = value.trim()
        }
    }
    if(query.includes("<")){
        const key = query.split("<")[0].trim()
        const value = query.split("<")[1]
        queryObject[key] = {$lt:  +value}
    }
    if(query.includes(">")){
        const key = query.split(">")[0].trim()
        const value = query.split(">")[1]
        queryObject[key] = {$gt: +value}
    }

    return queryObject

}
