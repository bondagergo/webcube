import * as cheerio from "cheerio"
import { getRequest } from "./httpRequest.js"

export interface ingatlancomPage {
    link: string,
    name: string,
    price: number,
    size: number,
    numberOfRooms: number,
    propertyStatus: string,
    buildYear: string,
    comfort: string,
    energyLevel: string,
    level: string,
    maxLevel: string,
    elevator: string,
    innerHeight: string,
    heatingType: string,
    airCondition: string,
    overhead: string,
    accessible: string,
    bathroomToilet: string,
    orientation: string,
    view: string,
    balconySize: string,
    gardenConnection: string,
    attic: string,
    parking: string,
    longDescription:string,
    photos:Object,
    sellerName: string,
    sellerPhone: Array<string>

}
/*
 [{
        size1920x1080: string,
        size800x600: string,
        size228x171: string,
        size80x60: string

    }]
    */

export interface ingatlancomListingJSON {

}



export function parseSearchResult(html:string) {

    const $ = cheerio.load(html)
    let returnedLinks: Array<string | undefined> = []
    const maxnumber = $("div.pagination__page-number").text().split("/")[1].replace(/\D/g, "")
    $("a.listing__link").map((index,element) => { returnedLinks.push($(element).attr("href")?.toString()) })
    return {returnedLinks , maxnumber}
    
}

export async  function parseRealEstatePage(url: string,html:string): Promise<ingatlancomPage> {
    const $ = cheerio.load(html)
    const spanArray: Array<any> = []
    const tableDataArray: Array<any> = []
    const listingId = url?.split("/")[url.split("/").length-1]
    console.log(listingId)
    const listingJson =  JSON.parse(await getRequest("https://sx-api.ingatlan.com/api/listings/" + listingId))
    const realtorJson = JSON.parse(await getRequest("https://sx-api.ingatlan.com/api/contact-phone-numbers/" + listingId + "?chunked=false"))
    let realtorPhone: Array<string> = []
    if (realtorJson.numbers){
        realtorPhone = realtorJson.numbers.map(e => {return e})
    }
    //const photosArray: Array<string> = []

    const photos = listingJson.photos.map(e => {return e.urls}) //return e.urls
    listingJson.photos.forEach(e => {e.urls})

    try {
        $("div.parametersContainer > div.parameter > div.parameterValues ").each((i,el) => {
            const spans = $(el).find("span")
            const priceSizeNumOfRooms = $(spans[0]).text()
            const row = {priceSizeNumOfRooms}
            spanArray.push(row)})
        $("dd.parameterValue").each((i,el)=> {
            const tableData = $(el)
            const tableDataTexted = $(tableData[0]).text()
            const tableDataInRow = {tableDataTexted}
            tableDataArray.push(tableDataInRow)
        })
        const parsedPage: ingatlancomPage = {
            link: url.trim(),
            name: $("h1.address").text().trim() + " " + $("div.digest").text().trim(),
            price: +spanArray[0].priceSizeNumOfRooms.trim().replace(',','.').replace(/[^\d.-]/g,''),
            size: +spanArray[1].priceSizeNumOfRooms.trim().replace(/\D/g,''),
            numberOfRooms: +spanArray[2].priceSizeNumOfRooms.trim().replace(/\D/g,''),
            propertyStatus: tableDataArray[0].tableDataTexted.trim(),
            buildYear: tableDataArray[1].tableDataTexted.trim(),
            comfort: tableDataArray[2].tableDataTexted.trim(),
            energyLevel: tableDataArray[3].tableDataTexted.trim(),
            level: tableDataArray[4].tableDataTexted.trim(),
            maxLevel: tableDataArray[5].tableDataTexted.trim(),
            elevator: tableDataArray[6].tableDataTexted.trim(),
            innerHeight: tableDataArray[7].tableDataTexted.trim(),
            heatingType: tableDataArray[8].tableDataTexted.trim(),
            airCondition: tableDataArray[9].tableDataTexted.trim(),
            overhead: tableDataArray[10].tableDataTexted.trim(),
            accessible: tableDataArray[11].tableDataTexted.trim(),
            bathroomToilet: tableDataArray[12].tableDataTexted.trim(),
            orientation: tableDataArray[13].tableDataTexted.trim(),
            view: tableDataArray[14].tableDataTexted.trim(),
            balconySize: tableDataArray[15].tableDataTexted.trim(),
            gardenConnection: tableDataArray[16].tableDataTexted.trim(),
            attic: tableDataArray[17].tableDataTexted.trim(),
            parking: tableDataArray[18].tableDataTexted.trim(),
            longDescription: $("div.longDescription").text().trim(),
            photos: photos,
            sellerName: $("div.name").text().trim(),
            sellerPhone: realtorPhone

        }
        return parsedPage
    } catch (error) {
        console.log("pageParser:  " + url)
        console.log(error)
        return {link:"",name:"",price:0,size:0,numberOfRooms:0,propertyStatus:"",buildYear:"",comfort:"",
        energyLevel:"",level:"",maxLevel:"",elevator:"",innerHeight:"",heatingType:"",airCondition:"",
        overhead:"",accessible:"",bathroomToilet:"",orientation:"",view:"",balconySize:"",gardenConnection:"",attic:"",
        parking:"",longDescription:"",photos:{},sellerName:"",sellerPhone:[]} // [{size1920x1080:"",size800x600:"",size228x171:"",size80x60:""}]
    }
   
    

}
