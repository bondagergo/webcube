import { v4 as uuidv4 } from 'uuid';

import {kafka,producer}  from '../kafka/producer';
//import * as Consumer from '../kafka/consumer';

export async function sendChatMessage(message:string, topic:string, user:string) {
    await producer.connect()
    await producer.send({topic: topic, messages:[{value: JSON.stringify({message, user})}]})
    const consumer = kafka.consumer({ groupId: uuidv4() });
    await consumer.connect()
    await consumer.subscribe({topic,fromBeginning: true})


    await consumer.run({
        // eachBatch: async ({ batch }) => {
        //   console.log(batch)
        // },
        eachMessage: async ({ topic, partition, message }) => {
          const prefix = `${topic}[${partition} | ${message.offset}] / ${message.timestamp}`
          console.log(`- ${prefix} ${message.key}#${message.value}`)
        },
      })

}
    
