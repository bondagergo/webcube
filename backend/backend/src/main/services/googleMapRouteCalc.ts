import {Client, TravelMode} from "@googlemaps/google-maps-services-js";
interface GoogleMapRoutesInput {
    startLocation: string,
    endLocation: string
}

export async function googleMapRouteCalc({startLocation, endLocation}:GoogleMapRoutesInput): Promise<string> {
   // const directionsService = new google.maps.DirectionsService();
   const client = new Client({});
   const apikey = process.env.GOOGLE_MAPS_API_KEY || 'nincs'
 //  let traveltime
 //    client.directions({
 //        params: {
 //            origin: startLocation,
 //            destination: endLocation,
 //            mode: TravelMode.driving,
 //            key: apikey
 //        }
 //    }).then((r) => {
 //      // console.log(r.data.routes[0].legs[0].duration);
 //       traveltime = r.data.routes[0].legs[0].duration.text
 //       return r.data.routes[0].legs[0].duration.text
 //     })
   const travelResult = await client.directions({
    params: {
        origin: startLocation,
        destination: endLocation,
        mode: TravelMode.driving,
        key: apikey
    }
})
//console.log(travelResult)
return travelResult.data.routes[0].legs[0].duration.text
      
}


