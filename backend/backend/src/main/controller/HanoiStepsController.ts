import { Controller } from "./BaseController"
import { Request, Response } from "express"
import { SolveSteps } from "../services/GameService"

export interface HanoiRequest extends Request {
    body:{
        hanoilvl: number
    }
  }


export class HanoiStepsController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.testfunc.bind(this))
    }


    async testfunc(req: HanoiRequest, res: Response) {

        const startTower: string = 'A'
        const helpTower: string = 'B'
        const endTower: string = 'C'

        const hanoilvl: number = req.body.hanoilvl
        const response :Array<string> =await SolveSteps([],hanoilvl,startTower,endTower,helpTower)
        console.log(response.toString())
       return res.json({cheatSheet: response})
    }
}