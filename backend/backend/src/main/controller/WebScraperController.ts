import { Controller } from "./BaseController"
import { Request, Response } from "express"
import {getRequest} from "../services/httpRequest"
import { parseSearchResult} from "../services/ingatlancComParser"
import { SaveToMongo } from "../mongoose/saveToMongo"
import { linkCollector, parsePages } from "../services/crawlIngatlanCom" 
import ingatlanComSchema from "../mongoose/schemas/ingatlanComSchema"



export interface WebScraperRequest extends Request {
    body:{
        url: string
    }
  }


export class WebScraperController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.scraperControl.bind(this))
    }


    async scraperControl(req: WebScraperRequest, res: Response) {



        const urlToSave: string = req.body.url
   
        const {linksToVisit,maxnumber } = await linkCollector(urlToSave)  //maxnumber


        for (let index = 2; index <= +maxnumber; index++) {
            console.log(index)
            let urlToSave: string = req.body.url.slice(0,-1) + index.toString()
            let pageHtml = await getRequest(urlToSave)
            let {returnedLinks} = parseSearchResult(pageHtml)
            linksToVisit.push(... returnedLinks)
            
        }

        const parsedPage = await parsePages(linksToVisit)
        console.log(parsedPage.length)
        console.log(parsedPage)
        
        await SaveToMongo(parsedPage,'ingatlanok','ingatlancom',ingatlanComSchema)
        
        
        //saveHtml(await pageHtml)
        //const response :Array<string> =await SolveSteps([],hanoilvl,startTower,endTower,helpTower)
        //console.log(response.toString())
       //return res.json({cheatSheet: response})
    }
}