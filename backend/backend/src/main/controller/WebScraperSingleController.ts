import { Controller } from "./BaseController"
import { Request, Response } from "express"
import { SaveToMongo } from "../mongoose/saveToMongo"
import ingatlanComSchema from "../mongoose/schemas/ingatlanComSchema"
import { getRequest } from "../services/httpRequest"
import { parseRealEstatePage } from "../services/ingatlanComParserNew"

export class WebScraperSingleaController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.webScraperSingleControl.bind(this))
    }

    

    async webScraperSingleControl(req: Request,res: Response) {

        const pageHtml = await getRequest(req.body.realEstateURL)
        const parsedPage = await parseRealEstatePage(req.body.realEstateURL,pageHtml)
        const parsedArray: Array<Object> = []
        parsedArray.push(parsedPage)
        console.log(parsedArray)
        await SaveToMongo(parsedArray,'ingatlanok','ingatlancom',ingatlanComSchema)

        
    }

}