import { Controller } from "./BaseController"
import { Request, Response } from "express"
import { kafkaConsume, kafkaProduce } from "../kafka/kafkatest"
//import { sendChatMessage } from "../kafka/kafkaService";

export class KafkaSendController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.kafkaChatSendControl.bind(this))
    }

    

    async kafkaChatSendControl(req: Request,res: Response) {

        //const sender = req.body.sender;
       // const topic = req.body.topic;
       // const messageContent = req.body.messageContent;
        //await sendChatMessage(sender,topic,messageContent)
        console.log(req.body.sender)
        await kafkaProduce()
        await kafkaConsume()
        return res.json({Kafka: 'response'})
        
    }

}