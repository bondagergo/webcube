import { Controller } from "./BaseController"
import { Request, Response } from "express"
import { simplequery } from "../mongoose/getDataFromMongo"
import { prepareIngatlanComQuery } from "../services/prepareIngatlanComQuery"
import ingatlanComSchema from "../mongoose/schemas/ingatlanComSchema"


export class RealEstateController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.ingatlanComControl.bind(this))
    }

    async ingatlanComControl(req: Request,res: Response) {
        console.log(req.body.query)
        const preparedQuery = prepareIngatlanComQuery(req.body.query)
        console.log(preparedQuery)
        const response = await simplequery(preparedQuery,'ingatlanok','ingatlancom',ingatlanComSchema) //JSON.parse(req.body.query)
        return res.json({realEstates: response})
    }

}