import * as express from "express"

/**
 * Base controller to be extended
 */
export class Controller {
    /**
     * Controller specific router
     */
    router: express.Express

    /**
     * Generates an empty express router
     */
    constructor() {
        this.router = express()
    }
}