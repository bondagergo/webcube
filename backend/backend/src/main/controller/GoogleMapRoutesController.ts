import { Controller } from "./BaseController"
import { Request, Response } from "express"
import { googleMapRouteCalc } from "../services/googleMapRouteCalc"

export interface GoogleMapRoutesState  
{
    startLocation: string,
    endLocation: string,
    travelTime: string
}

export class GoogleMapRoutesController extends Controller {

    constructor(){
        super()
        this.router.post("/get",this.googleMapRoutesControl.bind(this))
    }

    

    async googleMapRoutesControl(req: Request,res: Response) {
      //  interface GoogleMapRoutesState  
      //      {
      //          startLocation: string,
      //          endLocation: string,
      //          travelTime: string
      //      }
      console.log(req.body.startLocation)
      console.log(req.body.endLocation)

                        
        const response: GoogleMapRoutesState = 
        {
            startLocation: req.body.startLocation,
            endLocation: req.body.endLocation,
            travelTime: await googleMapRouteCalc({startLocation: req.body.startLocation,endLocation: req.body.endLocation })
        }

        console.log(response)
        return res.json(response)
        
    }

}