import * as express from "express"
import { Application} from "express"

export const expressBase =  (api: Application): Application =>
{

    const app = express()

    app.use(express.json())
    app.use(express.urlencoded({extended: true}))
    app.use("/api", api)

    return app
}