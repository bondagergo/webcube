import { createAction } from "@reduxjs/toolkit"
import CVCreatorGridProps from "interfaces/CVCreator/cvCreatorGridProps"



export const setCVGridNumberOfRows = createAction<number>('cvgrid/setNumberOfRows')
export const setCVGridDimensions = createAction<CVCreatorGridProps>('cvgrid/setdimensions')