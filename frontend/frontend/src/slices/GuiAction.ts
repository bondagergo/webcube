import { createAction } from "@reduxjs/toolkit"

export interface navBarTitleByURL {
    isSubPage: boolean,
    URLPath: string
}

export const setNavbarTitleByURL = createAction<navBarTitleByURL>('navbar/title')