export default interface gridChild{
    positionInGrid: Array<number>,
    positionInCol?: number,
    child: string//JSX.Element
}