import  gridChild  from "./gridChild";
import  rowLayout  from "./rowLayout";

export default interface CVCreatorGridProps {
    gridLayout:{
        numberOfRows: number,
        rowLayouts: Array<rowLayout>      
    },
    children?:Array<gridChild>
}