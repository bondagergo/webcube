import { colLayout } from "./colLayout";

export default interface rowLayout{
    rowId: number,
    rowProperty?: string,
    numberOfCols: number
    colLayouts?: Array<colLayout>
    }