import { Routes } from "./Routes"

export function InnerApp(): JSX.Element {

    return(
        <Routes />
    )
}