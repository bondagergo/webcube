import styled from "styled-components";
import { Navbar, Nav, NavDropdown, Container } from "react-bootstrap";

/* Connect all the comp names with the different selectors */

export const ExtraNavbar = styled.nav`
margin-right: 100px;
background-color: transparent;
border-bottom: 3px solid black;
@media only screen and (max-width: 768px) {}
`
export const MenuAnchor = styled.a`
color: black;
text-decoration: none;
@media only screen and (max-width: 768px) {
    display: block;
    padding: 15px 5px;
}
`

export const UlMenu = styled.ul`
list-style-type: none;
align-items: flex-start;     
flex-wrap: nowrap;
background: none;
@media only screen and (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
}

`

export const LiLogo = styled.li`
font-size: 20px;
padding: 7.5px 10px 7.5px 0;
order: 0;
`

export const LiItem = styled.li`
padding: 10px;
order: 1;
position: relative;
display: block; 
width: auto;
@media only screen and (max-width: 768px) {
order: 3;
width: 100%;
text-align: center;
display: none;
}
`

export const LiHasSubmenu = styled.li`
@media only screen and (max-width: 768px) {
font-size: 12px;
}
`

export const UlSubmenu = styled.ul`
list-style-type: none;
display: block;
position: absolute;
left: 0;
top: 68px;
background: #111;
@media only screen and (max-width: 768px) {
display: none;
}
`

export const LiSubmenuItem = styled.li`

`
export const LiToggle = styled.li`

`