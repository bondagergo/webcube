import React from 'react';
import { useSelector } from "react-redux";
import { RootState } from "../store";
import { ExtraNavbar, LiHasSubmenu, LiItem, LiLogo, LiSubmenuItem, LiToggle, MenuAnchor, UlMenu, UlSubmenu } from './layout/NavbarComponents';

export function FlexNavbar(): JSX.Element { 
    const {isSubPage, navSubTitle} = useSelector((state: RootState) => state.gui)


    /* Toggle mobile menu */

    /* JS FUNCTION HERE -- instead of selector onClick function call ?*/

    /* Activate Submenu */

    /*JS FUNCTION HERE toggleItem -- read desc about for cycle / instead of selector onClick function call ?*/

    return(
        <ExtraNavbar>
            <UlMenu>
                <LiLogo><a href="#">BGNetwork {isSubPage ? "/  " : null} {navSubTitle} </a></LiLogo>
                <LiItem><a href="#">Home</a></LiItem>
                <LiItem><a href="#">About</a></LiItem>
                <LiHasSubmenu>
                    <MenuAnchor tabIndex={0}>WebCrawling</MenuAnchor>
                    <UlSubmenu>
                        <LiSubmenuItem><a href="/webcrawler">WebCrawler</a></LiSubmenuItem>
                        <LiSubmenuItem><a href="/realestates">Real Estates</a></LiSubmenuItem>
                    </UlSubmenu>
                </LiHasSubmenu>
                <LiHasSubmenu>
                    <MenuAnchor tabIndex={0}>CV</MenuAnchor>
                    <UlSubmenu>
                        <LiSubmenuItem><a href="/cvcreator">CV creator</a></LiSubmenuItem>
                    </UlSubmenu>
                </LiHasSubmenu>
                <LiHasSubmenu>
                    <MenuAnchor tabIndex={0}>Games</MenuAnchor>
                    <UlSubmenu>
                        <LiSubmenuItem><a href="/hanoigame">HanoiGame</a></LiSubmenuItem>
                        <LiSubmenuItem><a href="/hanoicheatsheet">HanoiCheatSheet</a></LiSubmenuItem>
                    </UlSubmenu>
                </LiHasSubmenu>
                <LiToggle><a href="#"><i className="fas fa-bars"></i></a></LiToggle>

            </UlMenu>
        </ExtraNavbar>
    )
   

}