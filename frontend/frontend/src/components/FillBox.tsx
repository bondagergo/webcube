import { Container } from "react-bootstrap"

export const FillBox= (uniquekey: number): JSX.Element => {
    return(
        <Container key={uniquekey.toString()} fluid/>
    )
}